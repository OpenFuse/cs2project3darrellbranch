package edu.westga.cs1302.mathpractice.view;

import edu.westga.cs1302.mathpractice.view.resources.Responses;
import edu.westga.cs1302.mathpractice.viewmodel.MathPracticeViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * The Class MathPracticeGuiCodeBehind.
 * 
 * @author Darrell Branch
 */
public class MathPracticeGuiCodeBehind {

	/** The name text field. */
	@FXML
	private TextField nameTextField;
	
	/** The answer text field. */
	@FXML
	private TextField answerTextField;
	
	/** The start button. */
	@FXML
	private Button startButton;
	
	/** The submit button. */
	@FXML
	private Button submitButton;
	
	/** The next button. */
	@FXML
	private Button nextButton;
	
	/** The response label. */
	@FXML
	private Label responseLabel;
	
	/** The problem label. */
	@FXML
	private Label problemLabel;
	
	/** The invalid Name label. */
	@FXML
	private Label invalidNameLabel;
	
	/** The Name label. */
	@FXML
	private Label nameLabel;
	
	/** The score label. */
	@FXML
	private Label scoreLabel;
	
	@FXML
	private ListView<String> userListView;
	
	@FXML
	private ComboBox<String> difficultyDropdown;
	
	private MathPracticeViewModel viewModel;
	
	private Responses responses;
	
	/**
	 * Instantiates a new math practice gui code behind.
	 */
	public MathPracticeGuiCodeBehind() {
		this.viewModel = new MathPracticeViewModel();
		this.responses = new Responses();
		this.userListView = new ListView<String>();
		this.difficultyDropdown = new ComboBox<String>();
	}
	
	/**
	 * Initialize.
	 */
	@FXML
	private void initialize() {
		this.responseLabel.setVisible(false);
		this.invalidNameLabel.setVisible(false);
		this.problemLabel.textProperty().bind(this.viewModel.getProblem());
		this.nameTextField.textProperty().bindBidirectional(this.viewModel.getNameProperty());
		this.submitButton.disableProperty().set(true);
		this.nextButton.disableProperty().set(true);
		this.nameLabel.setVisible(false);
		this.scoreLabel.setVisible(false);
		this.userListView.itemsProperty().bind(this.viewModel.getUserListProperty());
		this.setListenersForAnswer();
		this.setListenersForListView();
		this.difficultyDropdown.getItems().add("EASY");
		this.difficultyDropdown.getItems().add("MEDIUM");
		this.difficultyDropdown.getItems().add("HARD");
		this.difficultyDropdown.getSelectionModel().selectFirst();
		this.viewModel.changeDifficulty(this.difficultyDropdown.getSelectionModel().getSelectedItem());
	}
	
	@FXML
	private void handleStart() {
		if (this.nameTextField.getText() != null && !this.nameTextField.getText().equals("")) {
			this.viewModel.startPractice();
			this.invalidNameLabel.setVisible(false);
			this.viewModel.setName();
			this.getNewProblem();
			this.submitButton.disableProperty().set(false);
			this.nextButton.disableProperty().set(false);
			this.nameLabel.setVisible(true);
			this.scoreLabel.setVisible(true);
			this.nameLabel.textProperty().bind(this.viewModel.getNameProperty());
			this.scoreLabel.textProperty().bind(this.viewModel.getScoreProperty());
			this.difficultyDropdown.disableProperty().set(true);
		} else {
			this.invalidNameLabel.setVisible(true);
		}
		
	}
	
	@FXML
	private void getNewProblem() {
		this.answerTextField.setText("");
		this.viewModel.nextProblem();
	}
	
	@FXML
	private void handleSubmit() {
		if (!this.answerTextField.getText().equals("")) {
			this.viewModel.setAnswer(this.answerTextField.getText());
			this.responseLabel.setVisible(true);
			
			boolean result = this.viewModel.checkAnswer().get();
			
			if (result) {
				this.responseLabel.setTextFill(Color.GREEN);
				this.responseLabel.setText(this.responses.getRandomPositiveResponse());
			} else {
				this.responseLabel.setTextFill(Color.RED);
				this.responseLabel.setText(this.responses.getRandomNegativeResponse());
			}
		}
		
		if (!this.viewModel.getSessionProperty().get()) {
			this.submitButton.disableProperty().set(true);
			this.nextButton.disableProperty().set(true);
			this.difficultyDropdown.disableProperty().set(false);
			this.saveUser();
		}
	}
	
	private void saveUser() {
		this.viewModel.saveCurrentUser();
	}

	private void setListenersForAnswer() {
		this.answerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			String pattern = "\\d*";
			newValue = newValue.replaceFirst("^0+(?!$)", "");
			if (!newValue.matches(pattern)) {
				this.answerTextField.setText(oldValue);
			} else {
				this.answerTextField.setText(newValue);
			}
			
		});
	}
	
	private void setListenersForListView() {
		this.userListView.getSelectionModel().selectedItemProperty().addListener((observable, oldUser, newUser) -> {
			if (newUser != null) {
				String[] userDetails = newUser.split(",");
				this.nameTextField.setText(userDetails[0]);
			}
		});
	}
	
	@FXML
	private void handleSaveAll() {
		this.viewModel.saveAllUsers();
	}
	
	@FXML
	private void handleChangeDifficulty() {
		this.viewModel.changeDifficulty(this.difficultyDropdown.getSelectionModel().getSelectedItem());
	}
	
}
