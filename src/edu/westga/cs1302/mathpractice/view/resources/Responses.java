package edu.westga.cs1302.mathpractice.view.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * The Class Responses.
 * 
 * @author Darrell Branch
 */
public class Responses {

	private ArrayList<String> positiveResponses;
	
	private ArrayList<String> negativeResponses;
	
	/**
	 * Instantiates a new responses.
	 */
	public Responses() {
		this.positiveResponses = new ArrayList<String>();
		this.negativeResponses = new ArrayList<String>();
		this.readResponsesFile();
	}

	private void readResponsesFile() {
		
		File inFile = new File("responses.txt");
		
		try (Scanner in = new Scanner(inFile)) {
			while (in.hasNextLine()) {
				String phrase = in.nextLine();
				if (phrase.contains("+")) {
					phrase = phrase.replace("+", "");
					this.positiveResponses.add(phrase);
				} else if (phrase.contains("-")) {
					phrase = phrase.replace("-", "");
					this.negativeResponses.add(phrase);
				}
			}
		} catch (Exception e) {
			System.err.print(e.getMessage());
			this.positiveResponses.add("Correct Answer");
			this.negativeResponses.add("Incorrect Answer");
			
		}		
	}
	
	/**
	 * Gets the random positive response.
	 *
	 * @return the random positive response
	 */
	public String getRandomPositiveResponse() {
		Random random = new Random();
		return this.positiveResponses.get(random.nextInt(this.positiveResponses.size()));
	}
	
	/**
	 * Gets the random negative response.
	 *
	 * @return the random negative response
	 */
	public String getRandomNegativeResponse() {
		Random random = new Random();
		return this.negativeResponses.get(random.nextInt(this.negativeResponses.size()));
	}
	
}
