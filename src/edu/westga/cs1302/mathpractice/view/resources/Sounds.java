package edu.westga.cs1302.mathpractice.view.resources;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * The Class Sounds.
 * 
 * @author Darrell Branch
 */
public class Sounds {
	
	private Clip clip;
	
	private AudioInputStream audioInputStream;

	/**
	 * Instantiates a new sounds.
	 */
	public Sounds() {
		this.loadSounds();
	}
	
	/**
	 * Load sounds.
	 */
	public void loadSounds() {
		try {
			this.audioInputStream = AudioSystem.getAudioInputStream(new File("cheer.wav"));
			this.clip = AudioSystem.getClip();
			this.clip.open(this.audioInputStream);
		} catch (Exception e) {
			System.err.print(e.getMessage());
		}
	}
	
	/**
	 * Play.
	 */
	public void play() {
		this.clip.setFramePosition(0);
		this.clip.start();
	}
	
	/**
	 * Stop.
	 */
	public void stop() {
		this.clip.stop();
	}
}
