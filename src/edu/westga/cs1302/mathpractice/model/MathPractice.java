package edu.westga.cs1302.mathpractice.model;

import java.util.Random;

/**
 * The Class MathPractice.
 * 
 * @author Darrell Branch
 */
public class MathPractice {

	/** The operand one. */
	private int operandOne;
	
	/** The operand two. */
	private int operandTwo;
	
	/** The answer. */
	private int answer;
	
	/** The formatted problem. */
	private String formattedProblem;
	
	private String user;
	
	private Integer problemsCorrect;
	
	private Integer problemsGenerated;
	
	private boolean sessionStarted;
	
	private Integer tries;
	
	private String difficulty;

	/**
	 * Instantiates a new math practice.
	 */
	public MathPractice() {
		this.sessionStarted = false;
	}
	
	/**
	 * Sets the difficulty.
	 *
	 * @param difficulty the new difficulty
	 */
	public void setDifficulty(String difficulty) {
		if (difficulty.equals("EASY") || difficulty.equals("MEDIUM") || difficulty.equals("HARD")) {
			this.difficulty = difficulty;
		}
		
		System.out.println(this.difficulty);
	}
	
	/**
	 * Start practice.
	 */
	public void startPractice() {
		this.problemsGenerated = 0;
		this.problemsCorrect = 0;
		this.sessionStarted = true;
	}

	/**
	 * Gets the operand one.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the operand one
	 */
	public int getOperandOne() {
		return this.operandOne;
	}

	/**
	 * Gets the operand two.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the operand two
	 */
	public int getOperandTwo() {
		return this.operandTwo;
	}

	/**
	 * Gets the answer.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the answer
	 */
	public int getAnswer() {
		return this.answer;
	}

	/**
	 * Gets the formatted problem.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the formatted problem
	 */
	public String getFormattedProblem() {
		return this.formattedProblem;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return this.user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the problems generated.
	 *
	 * @return the problems generated
	 */
	public Integer getProblemsGenerated() {
		return this.problemsGenerated;
	}
	
	/**
	 * Gets the problems correct.
	 *
	 * @return the problems correct
	 */
	public Integer getProblemsCorrect() {
		return this.problemsCorrect;
	}

	/**
	 * Checks if is session started.
	 *
	 * @return true, if is session started
	 */
	public boolean isSessionStarted() {
		return this.sessionStarted;
	}
	
	/**
	 * Generate math problem.
	 * 
	 * @precondition none
	 * @postcondition getOperandOne() == random; getOperandTwo() == random;
	 * 				  formattedProblem == getOperandOne() + " + " getOperandTwo() + " = ";
	 * 			      getAnswer() == getOperandOne() + getOperandTwo();
	 * 		          getProblemsGenerated()prev == getProblemsGenerated() - 1;
	 */
	public void generateMathProblem() {
		if (this.sessionStarted) {
			if (this.difficulty.equals("EASY")) {
				this.getEasyProblem();
			} else if (this.difficulty.equals("MEDIUM")) {
				this.getMediumProblem();
			} else {
				this.getHardProblem();
			}
			
			this.problemsGenerated++;
			this.tries = 0;
			
			if (this.problemsGenerated == 11) {
				this.sessionStarted = false;
			}
		}
	}
	
	private void getHardProblem() {
		Random random = new Random();
		boolean option = random.nextBoolean();
		this.operandOne = random.nextInt(1000);
		this.operandTwo = random.nextInt(1000);
		
		if (this.operandOne > this.operandTwo) {
			if (option) {
				this.answer = this.operandOne + this.operandTwo;
				this.formattedProblem = this.operandOne + " + " + this.operandTwo + " = ";
			} else {
				this.answer = this.operandOne - this.operandTwo;
				this.formattedProblem = this.operandOne + " - " + this.operandTwo + " = ";
			}
		} else {
			if (option) {
				this.answer = this.operandOne + this.operandTwo;
				this.formattedProblem = this.operandOne + " + " + this.operandTwo + " = ";
			} else {
				this.answer = this.operandTwo - this.operandOne;
				this.formattedProblem = this.operandTwo + " - " + this.operandOne + " = ";
			}
		}
	}

	private void getMediumProblem() {
		Random random = new Random();
		this.operandOne = random.nextInt(1000);
		this.operandTwo = random.nextInt(1000 - operandOne);
		this.answer = this.operandOne + this.operandTwo;
		this.formattedProblem = this.operandOne + " + " + this.operandTwo + " = ";
		
	}

	private void getEasyProblem() {
		Random random = new Random();
		this.operandOne = random.nextInt(50);
		this.operandTwo = random.nextInt(50);	
		this.answer = this.operandOne + this.operandTwo;
		this.formattedProblem = this.operandOne + " + " + this.operandTwo + " = ";
	}
	
	/**
	 * Process answer.
	 *
	 * @param userAnswer the answer
	 * @return true, if successful
	 */
	public boolean processAnswer(int userAnswer) {
		this.tries++;
		
		if (this.operandOne + this.operandTwo == userAnswer) {
			
			if (this.tries == 1) {
				this.addPoints();
			}
			return true;
			
		} else {
			this.subtractPoints();
			return false;
		}
	}

	private void subtractPoints() {
		if (this.difficulty.equals("EASY")) {
			this.problemsCorrect--;
		} else if (this.difficulty.equals("MEDIUM")) {
			this.problemsCorrect -= 2;
		} else {
			this.problemsCorrect -= 3;
		}
		
		if (this.problemsCorrect < 0) {
			this.problemsCorrect = 0;
		}
		
	}

	private void addPoints() {
		if (this.difficulty.equals("EASY")) {
			this.problemsCorrect++;
		} else if (this.difficulty.equals("MEDIUM")) {
			this.problemsCorrect += 2;
		} else {
			this.problemsCorrect += 3;
		}	
	}
	
}
