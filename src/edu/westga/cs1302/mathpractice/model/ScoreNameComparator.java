package edu.westga.cs1302.mathpractice.model;

import java.util.Comparator;

/**
 * The Class ScoreNameComparator.
 * 
 * @author Darrell Branch
 */
public class ScoreNameComparator implements Comparator<String> {

	/**
	 * Compares two users 
	 * 
	 * @return int
	 * @param entryOne the first
	 * @param entryTwo the second
	 */
	public int compare(String entryOne, String entryTwo) {
		String[] arrayOne = entryOne.split(",");
		String[] arrayTwo = entryTwo.split(",");
		Integer scoreOne = Integer.parseInt(arrayOne[1]);
		Integer scoreTwo = Integer.parseInt(arrayTwo[1]);
		
		if (scoreOne == scoreTwo) {
			return arrayOne[0].compareTo(arrayTwo[0]);
		} else if (scoreOne < scoreTwo) {
			return 1;
		} else if (scoreOne > scoreTwo) {
			return -1;
		} else {
			return 0;
		}
	}
}
