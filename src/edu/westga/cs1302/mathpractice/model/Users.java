package edu.westga.cs1302.mathpractice.model;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import javafx.collections.ObservableList;

/**
 * The Class Users.
 * 
 * @author Darrell Branch
 */
public class Users {
	private static final String USERS_FILE = "users.txt";
	private ArrayList<String> userList;
	
	/**
	 * Instantiates a new users.
	 */
	public Users() {
		this.userList = new ArrayList<String>();
		this.readUsers();
	}

	private void readUsers() {
		
		File inFile = new File(USERS_FILE);
		
		try (Scanner in = new Scanner(inFile)) {
			while (in.hasNextLine()) {
				String userData = in.nextLine();
				this.userList.add(userData);
			}
		} catch (Exception e) {
			System.err.print(e.getMessage());
		}
		
		Collections.sort(this.userList, new ScoreNameComparator());
		
	}
	
	/**
	 * Write users.
	 * 
	 * @param users the list of users
	 */
	public void writeUsers(ObservableList<String> users) {
		try (PrintWriter out = new PrintWriter(USERS_FILE)) {
			for (String currUser: users) {
				out.println(currUser);
			}
		} catch (IOException e) {
			System.err.print(e.getMessage());
		}
	}
	
	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public ArrayList<String> getUsers() {
		return this.userList;
	}
}
