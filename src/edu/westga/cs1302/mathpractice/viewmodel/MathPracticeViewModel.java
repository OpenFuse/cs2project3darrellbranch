package edu.westga.cs1302.mathpractice.viewmodel;

import java.util.Collections;

import edu.westga.cs1302.mathpractice.model.MathPractice;
import edu.westga.cs1302.mathpractice.model.ScoreNameComparator;
import edu.westga.cs1302.mathpractice.model.Users;
import edu.westga.cs1302.mathpractice.view.resources.Sounds;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The Class MathPracticeViewModel.
 * 
 * @author Darrell Branch
 */
public class MathPracticeViewModel {

	private StringProperty nameProperty;
	
	private StringProperty answerProperty;
	
	private StringProperty problemProperty;
	
	private StringProperty scoreProperty;
	
	private MathPractice mathPractice;
	
	private ListProperty<String> userListProperty;
	
	private Users users;
	
	private BooleanProperty sessionStartedProperty;
	
	private Sounds soundPlayer;
	
	/**
	 * Instantiates a new math practice view model.
	 */
	public MathPracticeViewModel() {
		this.nameProperty = new SimpleStringProperty();
		this.answerProperty = new SimpleStringProperty();
		this.problemProperty = new SimpleStringProperty();
		this.scoreProperty = new SimpleStringProperty();
		this.mathPractice = new MathPractice();
		this.sessionStartedProperty = new SimpleBooleanProperty();
		this.users = new Users();
		this.userListProperty = new SimpleListProperty<String>(FXCollections.observableArrayList(this.users.getUsers()));
		this.soundPlayer = new Sounds();
	}
	
	/**
	 * Next problem.
	 */
	public void nextProblem() {
		this.mathPractice.generateMathProblem();
		this.problemProperty.set(this.mathPractice.getFormattedProblem()); 
		this.sessionStartedProperty.set(this.mathPractice.isSessionStarted());
	}
	
	/**
	 * Gets the user list property.
	 *
	 * @return the user list property
	 */
	public ListProperty<String> getUserListProperty() {
		return this.userListProperty;
	}
	
	/**
	 * Sets the name.
	 */
	public void setName() {
		this.mathPractice.setUser(this.nameProperty.get());
	}
	
	/**
	 * Sets the answer.
	 *
	 * @param answer the new answer
	 */
	public void setAnswer(String answer) {
		this.answerProperty.set(answer);
	}
	
	/**
	 * Start practice.
	 */
	public void startPractice() {
		this.mathPractice.startPractice();
		this.mathPractice.generateMathProblem();
		this.scoreProperty.set(this.mathPractice.getProblemsCorrect().toString());
		this.sessionStartedProperty.set(this.mathPractice.isSessionStarted());
	}
	
	/**
	 * Gets the problem.
	 *
	 * @return the problem
	 */
	public StringProperty getProblem() {
		return this.problemProperty;
	}
	
	/**
	 * Gets the name property.
	 *
	 * @return the name property
	 */
	public StringProperty getNameProperty() {
		return this.nameProperty;
	}
	
	/**
	 * Gets the score property.
	 *
	 * @return the score property
	 */
	public StringProperty getScoreProperty() {
		return this.scoreProperty;
	}
	
	/**
	 * Gets the session property.
	 *
	 * @return the session property
	 */
	public BooleanProperty getSessionProperty() {
		return this.sessionStartedProperty;
	}
	
	/**
	 * Check answer.
	 *
	 * @return the boolean property
	 */
	public BooleanProperty checkAnswer() {
		boolean result = this.mathPractice.processAnswer(Integer.parseInt(this.answerProperty.get()));
		this.scoreProperty.set(this.mathPractice.getProblemsCorrect().toString());
		
		if (result) {
			this.soundPlayer.play();
		}
		
		return new SimpleBooleanProperty(result);
	}
	
	/**
	 * Save all users.
	 */
	public void saveAllUsers() {
		this.users.writeUsers(this.userListProperty.get());
	}

	/**
	 * Save current user.
	 */
	public void saveCurrentUser() {
		this.userListProperty.add(this.nameProperty.get() + "," + this.scoreProperty.get());
		Collections.sort(this.getUserListProperty().get(), new ScoreNameComparator());
	}
	
	/**
	 * Change difficulty.
	 *
	 * @param difficulty the difficulty
	 */
	public void changeDifficulty(String difficulty) {
		this.mathPractice.setDifficulty(difficulty);
	}
}
